###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false


config[:fonts_dir] = 'fonts'

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

# Templates for details


data.ps.courses.each do |course|
    proxy "/cursos/presenciales/#{course[:course_link]}/index.html", "/cursos/presenciales/course-detail.html", :locals => { :project => course }, :ignore => true
end



# Templates for register

data.ps.courses.each do |course|
    proxy "/cotizacion/presenciales/#{course[:course_link]}/index.html", "/cotizacion/presenciales/course-attend.html", :locals => { :project => course }, :ignore => true
end




# Templates for confirmation

data.ps.courses.each do |course|
    proxy "/confirmacion/presenciales/#{course[:course_link]}/index.html", "/confirmacion/presenciales/course-confirmation.html", :locals => { :project => course }, :ignore => true
end


# General configuration

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload

end

###
# Helpers
###

activate :directory_indexes
activate :asset_hash
set :relative_links, true

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Build-specific configuration
configure :build do
  # Relative assets
  activate :relative_assets

  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Minify html
  activate :minify_html, remove_input_attributes: false

end
